package org.apache.cordova.mediacapture.camera_activities;
import android.hardware.camera2.CameraAccessException;
import android.os.Bundle;

import org.apache.cordova.mediacapture.camera_activities.callbacks.CameraCaptureConfigured;
import org.apache.cordova.mediacapture.model.functional.Option;
import org.apache.cordova.mediacapture.R;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraDevice;

import android.view.Surface;
import android.view.View;

import java.io.File;
import java.util.ArrayList;


final public class VideoActivity extends CameraActivity {
    private int time = 0;
    private boolean isRecording;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void captureButtonOnClick(View view) {
        if (isRecording)
            stopRecordingVideo();
        else
            startRecordingVideo();
    }

    @Override
    protected String getFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
            + System.currentTimeMillis() + ".mp4";
    }

    private void startRecordingVideo() {
        if (!previewView.isAvailable())
            return;

        Option.ifPresent(openedCamera, previewSize, mediaRecorder, (camera, size, recorder) -> {
            CameraActivity.writeToLog("Video Button Clicked");
            try {
                closePreviewSession();
                setUpMediaRecorder();
                SurfaceTexture texture = previewView.getSurfaceTexture();
                assert texture != null;
                texture.setDefaultBufferSize(size.getWidth(), size.getHeight());
                previewBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                java.util.List<Surface> surfaces = new ArrayList<>();

                Surface previewSurface = new Surface(texture);
                surfaces.add(previewSurface);
                previewBuilder.addTarget(previewSurface);

                Surface recorderSurface = recorder.getSurface();
                surfaces.add(recorderSurface);
                previewBuilder.addTarget(recorderSurface);

                camera.createCaptureSession(surfaces, new CameraCaptureConfigured(session -> {
                    previewSession = Option.of(session);
                    updatePreview();
                    updateWhileRecordingUntilTimer();
                    runOnUiThread(() -> {
                        timer.setVisibility(View.VISIBLE);
                        recordButton.setBackground(R.drawable("white_square_button"));
                        recorder.start();
                    });
                }), backgroundTasks);
            } catch(CameraAccessException e) {
                e.printStackTrace();
            }
        });
    }

    private void updateWhileRecordingUntilTimer() {
        isRecording = true;
        new Thread(() -> {
            try {
                while (isRecording) {
                    time += 1;
                    setTimerText(time);
                    if (time == this.duration)
                        runOnUiThread(this::stopRecordingVideo);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void stopRecordingVideo() {
        isRecording = false;
        timer.setVisibility(View.INVISIBLE);
        time = 0;
        setTimerText(0);
        recordButton.setBackground(R.drawable("red_circle_button"));

        mediaRecorder.ifPresent(m -> {
            m.stop();
            m.reset();
        });
        finishActivity();
    }
}

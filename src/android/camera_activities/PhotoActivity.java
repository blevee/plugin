package org.apache.cordova.mediacapture.camera_activities;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;

import org.apache.cordova.mediacapture.R;
import org.apache.cordova.mediacapture.camera_activities.callbacks.CameraCaptureCallback;
import org.apache.cordova.mediacapture.camera_activities.callbacks.CameraCaptureConfigured;
import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.model.static_utils.Try;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class PhotoActivity extends CameraActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recordButton.setBackground(R.drawable("white_circle_button"));
    }

    @Override
    public void captureButtonOnClick(View view) {
        takePicture();
    }

    @Override
    protected String getFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/")) + System.currentTimeMillis() + ".jpg";
    }

    private void takePicture() {
        openedCamera.ifPresent(camera -> {
            CameraActivity.writeToLog("Picture Button Clicked");
            try {
                int width = fileSize.getWidth();
                int height = fileSize.getHeight();


                CameraActivity.writeToLog("Final w/h: " + width + " :: " + height);
                ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
                List<Surface> outputSurfaces = List.of(reader.getSurface(), new Surface(previewView.getSurfaceTexture()));
                final CaptureRequest.Builder captureBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                captureBuilder.addTarget(reader.getSurface());
                captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, orientation.orientationHint);
                nextAbsolutePath = getFilePath(this);
                final File file = new File(nextAbsolutePath);
                ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                    @Override
                    public void onImageAvailable(ImageReader reader) {
                        try (Image image = reader.acquireLatestImage()) {
                            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                            byte[] bytes = new byte[buffer.capacity()];
                            buffer.get(bytes);
                            Try.orThrow(() -> save(bytes));
                        }
                    }
                    private void save(byte[] bytes) throws IOException {
                        try (OutputStream output = new FileOutputStream(file)) {
                            output.write(bytes);
                        }
                    }
                };
                reader.setOnImageAvailableListener(readerListener, backgroundTasks);

                camera.createCaptureSession(outputSurfaces.toJavaList(), new CameraCaptureConfigured(session ->
                    session.capture(captureBuilder.build(), new CameraCaptureCallback(this::finishActivity), backgroundTasks)
                ), backgroundTasks);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        });
    }

}
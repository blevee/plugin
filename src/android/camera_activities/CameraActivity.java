package org.apache.cordova.mediacapture.camera_activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;

import org.apache.cordova.mediacapture.model.annotations.NonNull;
import org.apache.cordova.mediacapture.model.enumerations.Orientation;
import org.apache.cordova.mediacapture.camera_activities.callbacks.CameraCaptureConfigured;
import org.apache.cordova.mediacapture.model.functional.Function1;
import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.R;
import org.apache.cordova.mediacapture.model.functional.Option;
import org.apache.cordova.mediacapture.model.static_utils.Sizes;
import org.apache.cordova.mediacapture.model.static_utils.Try;
import org.apache.cordova.mediacapture.view.AutoFitTextureView;

import android.os.Build;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

abstract public class CameraActivity extends Activity {
    public static final String DIMENSION_TAG = "CameraActivity#dimension";
    // TODO: Move to separate log file
    private static BufferedWriter LOG_FILE_WRITER;

    public static void writeToLog(String data) {
        Try.orThrow(() -> {
            String download = Environment.getExternalStorageDirectory().getPath() + "/Download";
            File downloadF = new File(download);
            if(!downloadF.exists()){
                downloadF.mkdirs();
            }
            File logFile = new File(download, "CameraActivityLog.txt");

            LOG_FILE_WRITER = new BufferedWriter(new FileWriter(logFile, true));
            LOG_FILE_WRITER.write(data + "\n");
            LOG_FILE_WRITER.close();
        });
    }

    // ENDTODO
    public static final String DIMENSION_TYPE = "CameraActivity#dimension_type";
    public static final String DURATION_TAG = "CameraActivity#duration";
    public static final String TAG = "CameraActivity";

    private static final int DEFAULT_DURATION = 15;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_VIDEO_PERMISSIONS = 1;
    private static final List<String> VIDEO_PERMISSIONS = List.of(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    );

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);

        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    // TODO: Preview should likely be private and not protected
    protected Option<CameraDevice> openedCamera = Option.empty();
    protected Option<MediaRecorder> mediaRecorder = Option.empty();
    protected Option<Size> previewSize = Option.empty();
    protected Option<CameraCaptureSession> previewSession = Option.empty();

    protected CaptureRequest.Builder previewBuilder;
    protected TextView timer;
    protected AutoFitTextureView previewView;
    protected Button recordButton;
    protected Handler backgroundTasks;
    protected String nextAbsolutePath;
    protected int duration;
    protected Size fileSize;
    protected Orientation orientation = Orientation.PORTRAIT;

    // private List<Integer> validDimensions;
    private int actualDimension;
    private Sizes.Type dimensionType;
    private HandlerThread backgroundTasksThread;
    private Semaphore cameraLock = new Semaphore(1);

    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) { }
    };


    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            openedCamera = Option.of(cameraDevice);
            startPreview();
            cameraLock.release();
            if (null != previewView) {
                configureTransform(previewView.getWidth(), previewView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraLock.release();
            cameraDevice.close();
            openedCamera = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            cameraLock.release();
            cameraDevice.close();
            CameraActivity.this.openedCamera = null;
            CameraActivity.this.finish();
        }
    };

    abstract protected void captureButtonOnClick(View view);

    abstract protected String getFilePath(Context context);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        R.initR(getApplication());
        setContentView(R.layout("activity_camera"));

        Date date = new Date();
        writeToLog("\nStarted at: " + date.toString());

        if (savedInstanceState == null) {
            this.previewView = findViewById(R.id("texture"));
            this.recordButton = findViewById(R.id("video"));
            this.timer = findViewById(R.id("timer"));

            Intent intent = getIntent();
            this.dimensionType = Sizes.Type.valueOf(intent.getStringExtra(CameraActivity.DIMENSION_TYPE));
            this.actualDimension = intent.getIntExtra(CameraActivity.DIMENSION_TAG, 720);
//            setupDimensions(intent.getIntExtra(CameraActivity.DIMENSION_TAG, 720),
//                         intent.getStringExtra(CameraActivity.DIMENSION_TYPE));
            setupDuration(intent.getIntExtra(CameraActivity.DURATION_TAG, CameraActivity.DEFAULT_DURATION));
            setupRotation(findViewById(R.id("timerLayout")));

            setTimerText(0);
            this.recordButton.setOnClickListener(this::captureButtonOnClick);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();

        if (previewView.isAvailable())
            openCamera(previewView.getWidth(), previewView.getHeight());
        else
            previewView.setSurfaceTextureListener(surfaceTextureListener);
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_VIDEO_PERMISSIONS) {
            if (grantResults.length == VIDEO_PERMISSIONS.length()) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, getString(R.string("permission_request")), Toast.LENGTH_LONG).show();
                        break;
                    }
                }
            } else {
                Toast.makeText(this, getString(R.string("permission_request")), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startBackgroundThread() {
        backgroundTasksThread = new HandlerThread("CameraBackground");
        backgroundTasksThread.start();
        backgroundTasks = new Handler(backgroundTasksThread.getLooper());
    }

    private void stopBackgroundThread() {
        backgroundTasksThread.quitSafely();
        try {
            backgroundTasksThread.join();
            backgroundTasksThread = null;
            backgroundTasks = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean shouldShowRequestPermissionRationale(List<String> permissions) {
        return permissions.any(p -> ActivityCompat.shouldShowRequestPermissionRationale(this, p));
    }

    private void requestVideoPermissions() {
        if (!shouldShowRequestPermissionRationale(VIDEO_PERMISSIONS) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            requestPermissions(VIDEO_PERMISSIONS.toArray(String.class), REQUEST_VIDEO_PERMISSIONS);
    }

    private boolean hasPermissionsGranted(List<String> permissions) {
        return permissions.none(p -> ActivityCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED);
    }

    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {
        if (!hasPermissionsGranted(VIDEO_PERMISSIONS)) {
            requestVideoPermissions();
            return;
        }

        if (this.isFinishing())
            return;

        CameraManager manager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
        Try.orThrow(() -> {
            if (!cameraLock.tryAcquire(2500, TimeUnit.MILLISECONDS))
                throw new RuntimeException("Time out waiting to lock camera opening.");
            // TODO: Consider openedCamera.getID()
            String cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);


            if (map == null)
                throw new RuntimeException("Cannot get available preview/video sizes");

            Size[] sizes = dimensionType == Sizes.Type.PHOTO
                    ? map.getOutputSizes(ImageFormat.JPEG)
                    : map.getOutputSizes(MediaRecorder.class);

            this.fileSize = Sizes.chooseFileSize(List.fromArray(sizes), actualDimension, dimensionType);
            CameraActivity.writeToLog("Final fileSize: " + fileSize);
            Size size = Sizes.choosePreviewSize(List.fromArray(map.getOutputSizes(SurfaceTexture.class)), width, height, fileSize);
            this.previewSize = Option.of(size);
            previewView.setAspectRatio(size.getHeight(), size.getWidth());
//            int orientation = getResources().getConfiguration().orientation;

//            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                previewView.setAspectRatio(size.getHeight(), size.getWidth());
//            }
//            else {
//                previewView.setAspectRatio(size.getHeight(), size.getWidth());
//            }
//            configureTransform(width, height);
            mediaRecorder = Option.of(new MediaRecorder());
            manager.openCamera(cameraId, mStateCallback, null);
        });
    }

    private void closeCamera() {
        Try.orThrow(() -> {
            cameraLock.acquire();
            closePreviewSession();

            openedCamera.ifPresent(CameraDevice::close);
            openedCamera = Option.empty();

            mediaRecorder.ifPresent(MediaRecorder::release);
            mediaRecorder = Option.empty();
        }, () -> cameraLock.release());
    }

    protected void startPreview() {
        if (!previewView.isAvailable())
            return;

        Option.ifPresent(openedCamera, previewSize, (camera, size) -> Try.orThrow(() -> {
            closePreviewSession();
            SurfaceTexture texture = previewView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(size.getWidth(), size.getHeight());
            previewBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            Surface previewSurface = new Surface(texture);
            previewBuilder.addTarget(previewSurface);
            camera.createCaptureSession(List.of(previewSurface).toJavaList(), new CameraCaptureConfigured(session -> {
                previewSession = Option.of(session);
                updatePreview();
            }), backgroundTasks);
        }));
    }

    protected void updatePreview() {
        if (openedCamera.isEmpty())
            return;

        previewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        HandlerThread thread = new HandlerThread("CameraPreview");
        thread.start();
        previewSession.ifPresent(p -> Try.orThrow(() ->
            p.setRepeatingRequest(previewBuilder.build(), null, backgroundTasks)
        ));
    }

    protected void closePreviewSession() {
        previewSession.ifPresent(CameraCaptureSession::close);
        previewSession = Option.empty();
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        if (true) return;

        if (null == previewView) {
            return;
        }

        previewSize.ifPresent(size -> {
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            Matrix matrix = new Matrix();
            RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
            RectF bufferRect = new RectF(0, 0, size.getHeight(), size.getWidth());
            float centerX = viewRect.centerX();
            float centerY = viewRect.centerY();
            if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
                bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
                matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
                float scale = Math.max(
                    (float) viewHeight / size.getHeight(),
                    (float) viewWidth / size.getWidth());
                matrix.postScale(scale, scale, centerX, centerY);
                matrix.postRotate(90 * (rotation - 2), centerX, centerY);
            }
            previewView.setTransform(matrix);
        });
    }

    protected void setUpMediaRecorder() {
        mediaRecorder.ifPresent(m -> {
            m.setAudioSource(MediaRecorder.AudioSource.MIC);
            m.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            m.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            if (nextAbsolutePath == null || nextAbsolutePath.isEmpty()) {
                nextAbsolutePath = getFilePath(this);
            }
            m.setOutputFile(nextAbsolutePath);
            m.setVideoEncodingBitRate(10000000);
            m.setVideoFrameRate(30);
            m.setVideoSize(fileSize.getWidth(), fileSize.getHeight());
            m.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            m.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

            m.setOrientationHint(orientation.orientationHint);
            try {
                m.prepare();
            } catch (IOException e ) {
                e.printStackTrace();
            }
        });
    }

//    private void setupDimensions(int dimensionValue, String dimension) {
//        this.dimensionType = Sizes.Type.valueOf(dimension);
//        this.actualDimension = dimensionValue;
//
//        List<List<Integer>> dimensionLists;
//        final List<Integer> dims480 = List.of(480, 483);
//        final List<Integer> dims720 = List.of(640, 720, 768, 800);
//        final List<Integer> dims1080 = List.of(1024, 1050, 1080);
//        final List<Integer> dims1280 = List.of(1280);
//        final List<Integer> dims1600 = List.of(1600, 1680);
//        final List<Integer> dims1920 = List.of(1800, 1856, 1920, 2160);
//        dimensionLists = List.of(dims480, dims720, dims1080, dims1280, dims1600, dims1920);
//
//        List<Integer> defaultDimensionsList;
//        Log.d(CameraActivity.TAG, this.dimensionType.toString());
//        switch (this.dimensionType) {
//            case PHOTO:
//                defaultDimensionsList = dims1600;
//                break;
//            case VIDEO:
//                defaultDimensionsList = dims720;
//                break;
//            default:
//                throw new RuntimeException("Some case not handled");
//        }
//        this.validDimensions = dimensionLists.find(hs -> hs.contains(dimensionValue)).or(defaultDimensionsList);
//    }

    private void setupDuration(int duration) {
        this.duration = duration;
        if (this.duration == 0)
            this.duration = CameraActivity.DEFAULT_DURATION;
    }

    private void setupRotation(LinearLayout timerLayout) {
        // TODO: add destroy
        new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) timerLayout.getLayoutParams();

                orientation = Orientation.of(i);
                orientation.align(params);

                List.of(recordButton, timerLayout).forEach(v -> v.setRotation(orientation.rotationForElements)); // timer optional too
                timerLayout.setLayoutParams(params);
            }
        }.enable();
    }

    protected void finishActivity() {
        Intent result = new Intent();
        result.setData(Uri.fromFile(new File(nextAbsolutePath)));
        setResult(Activity.RESULT_OK, result);
        finish();
        nextAbsolutePath = null;
    }

    protected void setTimerText(int time) {
        Function1<Integer, String> fmt = n -> String.format("%02d", n);
        Function1<Integer, String> getTime = t -> {
            int minutes = t / 60;
            int seconds = t - (minutes * 60);
            return fmt.run(minutes) + ":" + fmt.run(seconds);
        };
        runOnUiThread(() -> timer.setText(getTime.run(time) + "/" + getTime.run(this.duration)));
    }
}

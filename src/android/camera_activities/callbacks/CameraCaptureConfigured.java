package org.apache.cordova.mediacapture.camera_activities.callbacks;

import android.hardware.camera2.CameraCaptureSession;

import org.apache.cordova.mediacapture.model.functional.ExceptionalConsumer1;

public class CameraCaptureConfigured extends CameraCaptureSession.StateCallback {
    private ExceptionalConsumer1<CameraCaptureSession> f;

    public CameraCaptureConfigured(ExceptionalConsumer1<CameraCaptureSession> f) {
        this.f = f;
    }

    @Override
    public void onConfigured(CameraCaptureSession session) {
        try {
            f.run(session);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onConfigureFailed(CameraCaptureSession session) { }
}

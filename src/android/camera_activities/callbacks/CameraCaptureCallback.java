package org.apache.cordova.mediacapture.camera_activities.callbacks;

import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;

import org.apache.cordova.mediacapture.model.functional.Consumer3;

public class CameraCaptureCallback extends CameraCaptureSession.CaptureCallback {
    private Consumer3<CameraCaptureSession, CaptureRequest, TotalCaptureResult> f;

    public CameraCaptureCallback(Runnable f) {
        this((session, request, result) -> f.run());
    }

    public CameraCaptureCallback(Consumer3<CameraCaptureSession, CaptureRequest, TotalCaptureResult> f) {
        this.f = f;
    }

    @Override
    public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
        super.onCaptureCompleted(session, request, result);
        f.run(session, request, result);
    }
}

package org.apache.cordova.mediacapture.test;

import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.model.static_utils.Sizes;

import static org.apache.cordova.mediacapture.model.static_utils.Sizes.InternalSize;
import static org.apache.cordova.mediacapture.model.static_utils.Sizes.Type;


import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class SizesTests {
    private List<InternalSize> sizes = List.of(
        new InternalSize(4032, 3024),
        new InternalSize(4032, 2268),
        new InternalSize(3840, 2160),
        new InternalSize(1920, 1080),
        new InternalSize(1920, 1440),
        new InternalSize(1600, 1200),
        new InternalSize(1280, 720),
        new InternalSize(1024, 768),
        new InternalSize(800, 600),
        new InternalSize(720, 480),
        new InternalSize(640, 480),
        new InternalSize(640, 360),
        new InternalSize(320, 240),
        new InternalSize(174, 144)
    );
    
    private List<InternalSize> s2 = List.of(
        new InternalSize(1440, 1080),
        new InternalSize(1088, 1088),
        new InternalSize(1280, 720),
        new InternalSize(1056, 704),
        new InternalSize(960, 720),
        new InternalSize(736, 736),
        new InternalSize(720, 480),
        new InternalSize(640, 480),
        new InternalSize(352, 288),
        new InternalSize(320, 240)
    );

    @Test void exactSize() {
        exactSizeHelper(sizes, 1080, Type.VIDEO, 1920, 1080);
        exactSizeHelper(sizes, 720,  Type.VIDEO, 1280, 720);
        exactSizeHelper(sizes, 1600, Type.PHOTO, 1600, 1200);
    }

    @Test void roundsUpEvenWhenBottomIsCloser() {
        exactSizeHelper(sizes, 1080, Type.PHOTO, 1600, 1200);
    }

    @Test void s2Test() {
        exactSizeHelper(s2, 1600, Type.PHOTO, 1440, 1080);
    }

    private void exactSizeHelper(List<InternalSize> sizes, int passedIn, Type type, int width, int height) {
        InternalSize s = Sizes.chooseFileSizeInternal(sizes, passedIn, type, false);
        assertEquals(width, s.getWidth());
        assertEquals(height, s.getHeight());
    }
}

package org.apache.cordova.mediacapture.test;
import org.apache.cordova.mediacapture.model.functional.Option;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class OptionTests {
    private Option<Integer> num = Option.of(4);
    private Option<Integer> empty = Option.empty();

    @Test void ifPresent() {
        AtomicInteger i = new AtomicInteger();
        num.ifPresent(i::set);
        empty.ifPresent(i::set);
        assertEquals(4, i.get());
        assertImmutableInvariants();
    }

    @Test void map() {
        assertTrue(num.map(x -> x * 2).contains(8));
        assertImmutableInvariants();
    }

    @Test void flatMap() {
        assertTrue(num.flatMap(x -> Option.of(x * 4)).contains(16));
        assertImmutableInvariants();
    }

    @Test void or() {
        assertEquals((Integer) 3, empty.or(3));
        assertNotEquals(4, empty.or(3));
        assertEquals((Integer) 3, empty.or(Option.of(3)).or(7));
        assertImmutableInvariants();
    }

    @Test void getOrThrow() {
        assertEquals((Integer) 4, num.getOrThrow());
        assertThrows(Exception.class, () -> empty.getOrThrow());
        assertImmutableInvariants();
    }

    @Test void contains() {
        assertTrue(num.contains(4));
        assertFalse(num.contains(3));
        assertImmutableInvariants();
    }

    @Test void isPresent() {
        assertTrue(num.isPresent());
        assertTrue(empty.isEmpty());
        assertImmutableInvariants();
    }

    void assertImmutableInvariants() {
        assertEquals(num, Option.of(4));
        assertEquals(empty, Option.empty());
    }
}

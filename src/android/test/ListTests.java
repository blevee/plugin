package org.apache.cordova.mediacapture.test;

import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.model.functional.Option;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ListTests {
    private List<Integer> nums = List.of(2,4,6,1,3,5);

    @Test void length() {
        assertEquals(6, nums.length());

        assertImmutableInvariants();
    }

    @Test void first() {
        assertTrue(nums.first().contains(2));

        assertImmutableInvariants();
    }

    @Test void last() {
        assertTrue(nums.last().contains(5));

        assertImmutableInvariants();
    }

    @Test void sortBy() {
        List<Integer> newNums = nums.sortBy(x -> x);
        assertTrue(newNums.first().contains(1));
        assertTrue(newNums.last().contains(6));

        assertImmutableInvariants();
    }

    @Test void map() {
        List<Integer> newNums = nums.map(x -> x * 2);

        assertEquals(6, nums.length());
        assertEquals(6, newNums.length());

        assertTrue(newNums.first().contains(4));
        assertTrue(newNums.last().contains(10));

        assertImmutableInvariants();
    }

    @Test void find() {
        assertTrue(nums.find(x -> x > 3).contains(4));

        assertImmutableInvariants();
    }

    @Test void filter() {
        assertEquals(3, nums.filter(x -> x > 3).length());
        assertEquals(6, nums.length());

        assertImmutableInvariants();
    }

    @Test void join() {
        assertEquals("2, 4, 6, 1, 3, 5", nums.join(", "));
        assertEquals("2,4,6,1,3,5", nums.join(","));

        assertImmutableInvariants();
    }

    @Test void toStringTest() {
        assertEquals("[2, 4, 6, 1, 3, 5]", nums.toString());

        assertImmutableInvariants();
    }

    @Test void fold() {
        assertEquals((Integer) 21, nums.fold((x,y) -> x + y, 0));

        assertImmutableInvariants();
    }

    @Test void closest() {
        assertTrue(Option.of(6d).contains(List.closest(nums, 7).getOrThrow()));
        List<Integer> nums2 = List.of(1, 5, 9, 14);
        assertTrue(Option.of(9d).contains(List.closest(nums2, 10).getOrThrow()));

        assertImmutableInvariants();
    }

    @Test void forEach() {
        AtomicInteger i = new AtomicInteger();
        nums.forEach(x -> i.getAndIncrement());
        assertEquals(6, i.get());

        assertImmutableInvariants();
    }

    @Test void append() {
        List<Integer> newNums = nums.append(4);
        assertEquals(6, nums.length());
        assertEquals(7, newNums.length());

        List<Integer> newNums2 = nums.append(List.of(3,4,5));
        assertEquals(9, newNums2.length());
        assertTrue(newNums2.last().contains(5));

        assertImmutableInvariants();
    }

    @Test void equals() {
        assertEquals(List.of(2,4,6,1,3,5), nums);

        assertImmutableInvariants();
    }

    @Test void toArray() {
        Integer[] newNums = nums.toArray(Integer.class);
        assertEquals(nums.first().getOrThrow(), newNums[0]);

        assertImmutableInvariants();
    }

    void assertImmutableInvariants() {
        assertEquals(List.of(2,4,6,1,3,5), nums);
    }
}

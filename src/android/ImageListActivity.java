package org.apache.cordova.mediacapture;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.cordova.LOG;
import org.apache.cordova.mediacapture.model.ImageSize;
import org.apache.cordova.mediacapture.network.DeviceInfo;
import org.apache.cordova.mediacapture.network.HttpConnector;
import org.apache.cordova.mediacapture.network.HttpEventListener;
import org.apache.cordova.mediacapture.network.ImageInfo;
import org.apache.cordova.mediacapture.network.StorageInfo;
import org.apache.cordova.mediacapture.view.ImageListArrayAdapter;
import org.apache.cordova.mediacapture.view.ImageRow;
import org.apache.cordova.mediacapture.view.LogView;
import org.apache.cordova.mediacapture.view.MJpegInputStream;
import org.apache.cordova.mediacapture.view.MJpegView;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Activity that displays the photo list
 */
public class ImageListActivity extends Activity {
	private ListView objectList;
	private LogView logViewer;
	private String cameraIpAddress;
	private ConnectivityManager cm;
	private ImageListArrayAdapter imageListArrayAdapter;
	private GLPhotoActivity glPhotoActivity;
	private List<ImageRow> imageRows;
	private ConnectivityManager.NetworkCallback netCallback;
	private int requestCode;
	private ArrayList<ImageInfo> objects;
	private LinearLayout layoutCameraArea;
	private Button btnShoot;
	private Button btnCancel;
	private TextView storageInfo;
	private TextView textCameraStatus;
	private MJpegView mMv;
	private boolean mConnectionSwitchEnabled = false;

	private LoadObjectListTask sampleTask = null;
	private ShowLiveViewTask livePreviewTask = null;
	private GetImageSizeTask getImageSizeTask = null;


    /**
     * onCreate Method
     * @param savedInstanceState onCreate Status value
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		R.initR(getApplication());
		imageRows = new ArrayList<>();

		requestCode = getIntent().getIntExtra("requestCode", 4);
		setContentView(R.layout("activity_object_list"));
		objectList = findViewById(R.id("object_list"));
		logViewer = (LogView) findViewById(R.id("log_view"));
		cameraIpAddress = getResources().getString(R.string("theta_ip_address"));
		storageInfo = findViewById(R.id("storage_info"));
		getActionBar().setTitle("Connect to Ricoh Theta");
		String package_name = getApplication().getPackageName();
		layoutCameraArea = (LinearLayout) findViewById(R.id("shoot_area"));
		textCameraStatus = (TextView) findViewById(R.id("camera_status"));
		btnShoot = (Button) findViewById(R.id("btn_shoot"));
		btnCancel = (Button) findViewById(R.id("btn_cancel"));
		btnShoot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				btnShoot.setEnabled(false);
				textCameraStatus.setText(R.string("text_camera_synthesizing"));
				new ShootTask().execute();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		mMv = (MJpegView) findViewById(R.id("live_view"));

		mConnectionSwitchEnabled = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		mMv.stopPlay();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mMv.play();

		if (livePreviewTask != null) {
			livePreviewTask.cancel(true);
			livePreviewTask = new ShowLiveViewTask();
			livePreviewTask.execute(cameraIpAddress);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == GLPhotoActivity.REQUEST_REFRESH_LIST) {
			if (sampleTask != null) {
				sampleTask.cancel(true);
			}
			sampleTask = new LoadObjectListTask();
			sampleTask.execute();
		}
		if (resultCode == 88) {
			setResult(-1);

			finish();
		}
	}

	@Override
	protected void onDestroy() {
		netCallback = null;
		cm = null;
		if (imageRows != null) {
			imageRows.removeAll(imageRows);
		}
		if (objects != null) {
			objects.removeAll(objects);
		}
		sampleTask.cancel(true);
		objectList = null;
		glPhotoActivity = null;
		imageListArrayAdapter = null;
		livePreviewTask = null;
		logViewer = null;
		if (sampleTask != null) {
			sampleTask.cancel(true);
		}

		if (livePreviewTask != null) {
			livePreviewTask.cancel(true);
		}

		super.onDestroy();
	}

	/**
	 * onCreateOptionsMenu Method
     * @param menu Menu initialization object
     * @return Menu display feasibility status value
     */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		R.initR(getApplication());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu("connection"), menu);

		Switch connectionSwitch = menu.findItem(R.id("connection")).getActionView().findViewById(R.id("connection_switch"));
		connectionSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
			if (objectList != null) {
				imageListArrayAdapter = new ImageListArrayAdapter(ImageListActivity.this, R.layout("listlayout_object"), new ArrayList<>());
				objectList.setAdapter(imageListArrayAdapter);
			} else {
				Log.d("LEAK", "Leaking");
			}

			if (isChecked) {
				layoutCameraArea.setVisibility(View.VISIBLE);

				if (sampleTask == null) {
					sampleTask = new LoadObjectListTask();
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							sampleTask.execute();
						}
					});
				}

				if (livePreviewTask == null) {
					livePreviewTask = new ShowLiveViewTask();
					livePreviewTask.execute(cameraIpAddress);
				}
			} else {
				layoutCameraArea.setVisibility(View.INVISIBLE);

				if (sampleTask != null) {
					sampleTask.cancel(true);
					sampleTask = null;
				}

				if (livePreviewTask != null) {
					livePreviewTask.cancel(true);
					livePreviewTask = null;
				}

				new DisConnectTask().execute();

				mMv.stopPlay();
			}
		});
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Switch connectionSwitch = menu.findItem(R.id("connection")).getActionView().findViewById(R.id("connection_switch"));
		if (!mConnectionSwitchEnabled) {
			connectionSwitch.setChecked(false);
		}
		connectionSwitch.setEnabled(mConnectionSwitchEnabled);
		return true;
	}

	private void changeCameraStatus(final int resid) {
		runOnUiThread(() -> textCameraStatus.setText(resid));
	}

	private void appendLogView(final String log) {
		runOnUiThread(() -> logViewer.append(log));
	}

	private class GetImageSizeTask extends AsyncTask<Void, String, ImageSize> {

		@Override
		protected ImageSize doInBackground(Void... params) {
			publishProgress("get current image size");
			HttpConnector camera = new HttpConnector(cameraIpAddress);
			ImageSize imageSize = camera.getImageSize();

			return imageSize;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}

		@Override
		protected void onPostExecute(ImageSize imageSize) {
			if (imageSize != null) {
			} else {
				logViewer.append("failed to get image size");
			}
		}
	}


	private class ShowLiveViewTask extends AsyncTask<String, String, MJpegInputStream> {
		@Override
		protected MJpegInputStream doInBackground(String... ipAddress) {
			MJpegInputStream mjis = null;
			final int MAX_RETRY_COUNT = 20;

			for (int retryCount = 0; retryCount < MAX_RETRY_COUNT; retryCount++) {
				try {
					if (sampleTask == null) {
						sampleTask = new LoadObjectListTask();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								sampleTask.execute();
							}
						});
					}
					publishProgress("start Live view");
					HttpConnector camera = new HttpConnector(ipAddress[0]);
					camera.getSessionId();
					camera.setClientVersion(2);
					InputStream is = camera.getLivePreview();
					mjis = new MJpegInputStream(is);
					retryCount = MAX_RETRY_COUNT;
				} catch (IOException e) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				} catch (JSONException e) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}

			return mjis;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				if (logViewer != null) {
					logViewer.append(log);
				}
			}
		}

		@Override
		protected void onPostExecute(MJpegInputStream mJpegInputStream) {
			if (mJpegInputStream != null) {
				mMv.setSource(mJpegInputStream);
			} else {
				logViewer.append("failed to start live view");
			}
		}
	}

	private class LoadObjectListTask extends AsyncTask<Void, String, List<ImageRow>> {

		private ProgressBar progressBar;

		public LoadObjectListTask() {
			progressBar = findViewById(R.id("loading_object_list_progress_bar"));
		}

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected List<ImageRow> doInBackground(Void... params) {
			try {
				publishProgress("------");
				publishProgress("connecting to " + cameraIpAddress + "...");
				HttpConnector camera = new HttpConnector(cameraIpAddress);
				changeCameraStatus(R.string("text_camera_standby"));

				DeviceInfo deviceInfo = camera.getDeviceInfo();
				publishProgress("connected.");
				publishProgress(deviceInfo.getClass().getSimpleName() + ":<" + deviceInfo.getModel() + ", " + deviceInfo.getDeviceVersion() + ", " + deviceInfo.getSerialNumber() + ">");

				StorageInfo storage = camera.getStorageInfo();
				ImageRow storageCapacity = new ImageRow();
				int freeSpaceInImages = storage.getFreeSpaceInImages();
				int megaByte = 1024 * 1024;
				long freeSpace = storage.getFreeSpaceInBytes() / megaByte;
				long maxSpace = storage.getMaxCapacity() / megaByte;
				storageCapacity.setFileName("Free space: " + freeSpaceInImages + "[shots] (" + freeSpace + "/" + maxSpace + "[MB])");
				imageRows.add(storageCapacity);

				objects = camera.getList();
				int objectSize = objects.size();

				for (int i = 0; i < objectSize; i++) {
					ImageRow imageRow = new ImageRow();
					ImageInfo object = objects.get(i);
					imageRow.setFileId(object.getFileId());
					imageRow.setFileSize(object.getFileSize());
					imageRow.setFileName(object.getFileName());
					imageRow.setCaptureDate(object.getCaptureDate());
					publishProgress("<ImageInfo: File ID=" + object.getFileId() + ", filename=" + object.getFileName() + ", capture_date=" + object.getCaptureDate()
							+ ", image_pix_width=" + object.getWidth() + ", image_pix_height=" + object.getHeight() + ", object_format=" + object.getFileFormat()
							+ ">");

					if (object.getFileFormat().equals(ImageInfo.FILE_FORMAT_CODE_EXIF_JPEG)) {
						imageRow.setIsPhoto(true);
						Bitmap thumbnail = camera.getThumb(object.getFileId());
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
						byte[] thumbnailImage = baos.toByteArray();
						try {
							baos.flush();
							baos.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						imageRow.setThumbnail(thumbnailImage);
						thumbnail.recycle();
						thumbnailImage = null;
					} else {
						imageRow.setIsPhoto(false);
					}
					imageRows.add(imageRow);
					imageRow = null;
					object = null;
					publishProgress("getList: " + (i + 1) + "/" + objectSize);
				}
				return imageRows;

			} catch (Throwable throwable) {
				String errorLog = Log.getStackTraceString(throwable);
				publishProgress(errorLog);
				return null;
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}

		@Override
		protected void onPostExecute(List<ImageRow> imageRows) {
			if (imageRows != null) {
				if (objectList != null) {
					String info = imageRows.get(0).getFileName();
					imageRows.remove(0);
					storageInfo.setText(info);
					HttpConnector camera = new HttpConnector(cameraIpAddress);

					imageListArrayAdapter = new ImageListArrayAdapter(ImageListActivity.this, R.layout("listlayout_object"), imageRows);
					objectList.setAdapter(imageListArrayAdapter);
					objectList.setOnItemClickListener((parent, view, position, id) -> {
						ImageRow selectedItem = (ImageRow) parent.getItemAtPosition(position);
						if (selectedItem.isPhoto()) {
							byte[] thumbnail = selectedItem.getThumbnail();
							String fileId = selectedItem.getFileId();
							if (camera != null) {
								glPhotoActivity.startActivityForResult(ImageListActivity.this, cameraIpAddress, fileId, thumbnail, false);
							} else {
								finish();
							}
						} else {
							Toast.makeText(getApplicationContext(), "This isn't a photo.", Toast.LENGTH_SHORT).show();
						}
					});
					objectList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
						private String mFileId;

						@Override
						public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
							ImageRow selectedItem = (ImageRow) parent.getItemAtPosition(position);
							mFileId = selectedItem.getFileId();
							String fileName = selectedItem.getFileName();

							new AlertDialog.Builder(ImageListActivity.this)
									.setTitle(fileName)
									.setMessage(R.string("delete_dialog_message"))
									.setPositiveButton(R.string("dialog_positive_button"), (dialog, which) -> {
										DeleteObjectTask deleteTask = new DeleteObjectTask();
										deleteTask.execute(mFileId);
									})
									.show();
							return true;
						}
					});
				} else {
				Log.d("LEAK", "Leaking");
				}
			} else {
				logViewer.append("failed to get image list");
			}

			progressBar.setVisibility(View.GONE);
		}

		@Override
		protected void onCancelled() {
			progressBar.setVisibility(View.GONE);
		}

	}

	private class DeleteObjectTask extends AsyncTask<String, String, Void> {

		@Override
		protected Void doInBackground(String... fileId) {
			publishProgress("start delete file");
			DeleteEventListener deleteListener = new DeleteEventListener();
			HttpConnector camera = new HttpConnector(getResources().getString(R.string("theta_ip_address")));
			camera.deleteFile(fileId[0], deleteListener);

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			logViewer.append("done");
		}

		private class DeleteEventListener implements HttpEventListener {
			@Override
			public void onCheckStatus(boolean newStatus) {
				if (newStatus) {
					appendLogView("deleteFile:FINISHED");
				} else {
					appendLogView("deleteFile:IN PROGRESS");
				}
			}

			@Override
			public void onObjectChanged(String latestCapturedFileId) {
				appendLogView("delete " + latestCapturedFileId);
			}

			@Override
			public void onCompleted() {
				appendLogView("deleted.");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						new LoadObjectListTask().execute();
					}
				});
			}

			@Override
			public void onError(String errorMessage) {
				appendLogView("delete error " + errorMessage);
			}
		}
	}

	private class DisConnectTask extends AsyncTask<Void, String, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			try {
				publishProgress("disconnected.");
				return true;

			} catch (Throwable throwable) {
				String errorLog = Log.getStackTraceString(throwable);
				publishProgress(errorLog);
				return false;
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}
	}

	private class ShootTask extends AsyncTask<Void, Void, HttpConnector.ShootResult> {

		@Override
		protected void onPreExecute() {
			logViewer.append("takePicture");
		}

		@Override
		protected HttpConnector.ShootResult doInBackground(Void... params) {
			mMv.setSource(null);
			CaptureListener postviewListener = new CaptureListener();
			HttpConnector camera = new HttpConnector(getResources().getString(R.string("theta_ip_address")));
			camera.setClientVersion(2);
			HttpConnector.ShootResult result = camera.takePicture(postviewListener);

			return result;
		}

		@Override
		protected void onPostExecute(HttpConnector.ShootResult result) {
			if (result == HttpConnector.ShootResult.FAIL_CAMERA_DISCONNECTED) {
				logViewer.append("takePicture:FAIL_CAMERA_DISCONNECTED");
			} else if (result == HttpConnector.ShootResult.FAIL_STORE_FULL) {
				logViewer.append("takePicture:FAIL_STORE_FULL");
			} else if (result == HttpConnector.ShootResult.FAIL_DEVICE_BUSY) {
				logViewer.append("takePicture:FAIL_DEVICE_BUSY");
			} else if (result == HttpConnector.ShootResult.SUCCESS) {
				logViewer.append("takePicture:SUCCESS");
			}
		}

		private class CaptureListener implements HttpEventListener {
			private String latestCapturedFileId;
			private boolean ImageAdd = false;

			@Override
			public void onCheckStatus(boolean newStatus) {
				if(newStatus) {
					appendLogView("takePicture:FINISHED");
				} else {
					appendLogView("takePicture:IN PROGRESS");
				}
			}

			@Override
			public void onObjectChanged(String latestCapturedFileId) {
				this.ImageAdd = true;
				this.latestCapturedFileId = latestCapturedFileId;
			}

			@Override
			public void onCompleted() {
				appendLogView("CaptureComplete");
				if (ImageAdd) {
					runOnUiThread(() -> {
						btnShoot.setEnabled(true);
						textCameraStatus.setText(R.string("text_camera_standby"));
						new GetThumbnailTask(latestCapturedFileId).execute();
					});
				}
			}

			@Override
			public void onError(String errorMessage) {
				appendLogView("CaptureError " + errorMessage);
				runOnUiThread(() -> {
					btnShoot.setEnabled(true);
					textCameraStatus.setText(R.string("text_camera_standby"));
				});
			}
		}

	}

	private class GetThumbnailTask extends AsyncTask<Void, String, Void> {

		private String fileId;

		public GetThumbnailTask(String fileId) {
			this.fileId = fileId;
		}

		@Override
		protected Void doInBackground(Void... params) {
			HttpConnector camera = new HttpConnector(getResources().getString(R.string("theta_ip_address")));
			Bitmap thumbnail = camera.getThumb(fileId);
			if (thumbnail != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				byte[] thumbnailImage = baos.toByteArray();
				try {
					baos.flush();
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				thumbnail.recycle();
				glPhotoActivity.startActivityForResult(ImageListActivity.this, cameraIpAddress, fileId, thumbnailImage, true);
			} else {
				publishProgress("failed to get file data.");
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}
	}
}

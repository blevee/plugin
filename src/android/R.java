package org.apache.cordova.mediacapture;

import android.app.Application;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class R {
    private static String packageName;
    private static Resources resources;

    public static int menu(String name) { return identifier(name, "menu"); }
    public static int id(String name) { return identifier(name, "id"); }
    public static int layout(String name) { return identifier(name, "layout"); }
    public static int string(String name) { return identifier(name, "string"); }

    public static Drawable drawable(String name) {
        return resources.getDrawable(identifier(name, "drawable"));
    }


    private static int identifier(String name, String type) {
        if (packageName == null || resources == null)
            throw new RuntimeException("R was not initialized properly, make sure you call `initR` in onCreate for activities that use R.");

        int id = resources.getIdentifier(name, type, packageName);

        if (id == 0)
            throw new RuntimeException("Invalid Resource, this either signals a typo or that the invalid R was setup.");

        return id;
    }

    public static void initR(Application application) {
        packageName = application.getPackageName();
        resources = application.getResources();
    }
}

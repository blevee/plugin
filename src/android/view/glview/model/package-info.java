/**
 * Package in which OpenGL objects used by glview are stored
 */
package org.apache.cordova.mediacapture.view.glview.model;
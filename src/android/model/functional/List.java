package org.apache.cordova.mediacapture.model.functional;

import org.apache.cordova.mediacapture.model.annotations.NonNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

// Immutable Linked List
public class List<T> {
    public static <T> List<T> of() {
        return new List<>();
    }

    public static <T> List<T> of(T ... xs) {
        return new List<>(java.util.Arrays.asList(xs));
    }

    public static <T> List<T> fromArray (T[] xs) {
        return new List<>(java.util.Arrays.asList(xs));
    }

    public static <T> List<T> of(java.util.List<T> xs) {
        return new List<>(xs);
    }

    public static <T, R extends Number> Option<T> closest(List<T> xs, Function1<T, R> f, R r, Function1<T, Boolean> invariant) {
        return xs.fold((item, closest) -> {
            double itemD = f.run(item).doubleValue();
            Option<Double> closestD = closest.map(c -> f.run(c).doubleValue());

            double itemDistance = Math.abs(itemD - r.doubleValue());
            double closestDistance = closestD.map(c -> Math.abs(c - r.doubleValue())).or(Double.MAX_VALUE);

            if (invariant.run(item))
                return closest.map(c -> itemDistance < closestDistance ? item : c).or(Option.of(item));
            else
                return closest;
        }, Option.empty());
    }

    public static <T, R extends Number> Option<T> closest(List<T> xs, Function1<T, R> f, R r) {
        return closest(xs, f, r, x -> true);
    }

    public static <T extends Number> Option<Double> closest(List<T> xs, T t) {
        return closest(xs, t, x -> true);
    }
    public static <T extends Number> Option<Double> closest(List<T> xs, T t, Function1<T, Boolean> invariant) {
        return xs.fold((item, closest) -> {
            double itemD = item.doubleValue();

            double itemDistance = Math.abs(itemD - t.doubleValue());
            double closestDistance = closest.map(c -> Math.abs(c - t.doubleValue())).or(Double.MAX_VALUE);

            if (invariant.run(item))
                return closest.map(c -> itemDistance < closestDistance ? itemD : c).or(Option.of(itemD));
            else
                return closest;
        }, Option.empty());
    }

    public int length() {
        AtomicInteger i = new AtomicInteger();
        forEach(x -> i.getAndIncrement());
        return i.get();
    }

    public Option<T> get(int i) {
        Option<Node<T>> node = head;

        for (int j = 0; j < i; j++)
            node = node.flatMap(Node::next);

        return node.map(Node::data);
    }

    public Option<T> first() {
        return get(0);
    }

    public Option<T> last() {
        return get(length() - 1);
    }

    public <R extends Comparable<R>> Option<T> minBy(@NonNull Function1<T, R> f) {
        return sortBy(f).first();
    }

    public Option<T> find(@NonNull Function1<T, Boolean> check) {
        return filter(check).first();
    }

    public boolean contains(T t) {
        return any(t::equals);
    }

    public boolean missing(T t) {
        return !contains(t);
    }

    public boolean any(@NonNull Function1<T, Boolean> check) {
        return filter(check).length() > 0;
    }

    public boolean none(@NonNull Function1<T, Boolean> check) {
        return filter(check).length() == 0;
    }

    public <R extends Comparable<R>> List<T> sortBy(@NonNull Function1<T, R> f) {
        AtomicReference<List<T>> newList = new AtomicReference<>(new List<>());
        forEach(x -> {
            R compareValue = f.run(x);
            List<T> lessThanList = newList.get().filter(y -> f.run(y).compareTo(compareValue) < 0);
            List<T> greaterThanOrEqualList = newList.get().filter(y -> f.run(y).compareTo(compareValue) >= 0);

            newList.set(lessThanList.append(x).append(greaterThanOrEqualList));
        });
        return newList.get();
    }

    public <R> List<R> map(@NonNull Function1<T, R> f) {
        AtomicReference<List<R>> newList = new AtomicReference<>(new List<>());
        forEach(x -> newList.set(newList.get().append(f.run(x))));
        return newList.get();
    }

    public List<T> filter(@NonNull Function1<T, Boolean> check) {
        AtomicReference<List<T>> newList = new AtomicReference<>(new List<>());
        forEach(x -> {
            if (check.run(x))
                newList.set(newList.get().append(x));
        });
        return newList.get();
    }

    public List<T> tap(@NonNull Consumer1<List<T>> f) {
        f.run(this);
        return this;
    }

    public List<T> drop(int i) {
        return new List<>(this).tap(l -> l.mutableDrop(i));
    }

    public List<T> append(@NonNull T x) {
        return new List<>(this).tap(l -> l.mutableAppend(x));
    }

    public List<T> append(@NonNull List<T> xs) {
        return new List<>(this).tap(l -> xs.forEach(l::mutableAppend));
    }

    public <R> R fold(Function2<T, R, R> f, R start) {
        AtomicReference<R> last = new AtomicReference<>();
        last.set(start);

        forEach(x -> last.set(f.run(x, last.get())));
        return last.get();
    }

    public void forEach(@NonNull Consumer1<T> f) {
        Option<Node<T>> node = head;

        while (node.isPresent()) {
            node.map(Node::data).ifPresent(f);
            node = node.flatMap(Node::next);
        }
    }

    public String join(@NonNull String separator) {
        StringBuilder sb = new StringBuilder();
        forEach(t -> sb.append(t).append(separator));
        String str = sb.toString();
        return str.length() > 0 ? str.substring(0, str.length() - separator.length()) : str;
    }

    public java.util.List<T> toJavaList() {
        java.util.List<T> list = new ArrayList<>();
        forEach(list::add);
        return list;
    }

    public T[] toArray(Class<T> klass) {
        T[] result = (T[]) Array.newInstance(klass, length());
        for (int i = 0; i < length(); i++)
            result[i] = get(i).getOrThrow();
        return result;
    }

    @Override
    public String toString() {
        return "[" + join(", ") + "]";
    }

    private Option<Node<T>> head = Option.empty();
    private Option<Node<T>> last = head;

    private List() { }

    private List(java.util.List<T> ts) {
        for (T item : ts)
            mutableAppend(item);
    }

    private List(List<T> oldList) {
        oldList.forEach(this::mutableAppend);
    }

    private void mutableDrop(int i) {
        if (i > 0) {
            head = head.flatMap(Node::next);
            mutableDrop(i - 1);
        }
    }

    private void mutableAppend(T value) {
        Option<Node<T>> newNode = Option.of(new Node<>(value));
        head.ifEmpty(() -> head = newNode);
        last.ifPresent(n -> n.next = newNode);
        last = newNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        List<?> list = (List<?>) o;
        return head.equals(list.head) && last.equals(list.last);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, last);
    }

    private static class Node<T> {
        @NonNull final T data;
        @NonNull Option<Node<T>> next;

        Node(@NonNull T data) {
            this.data = data;
            this.next = Option.empty();
        }

        @NonNull T data() { return data; }
        @NonNull Option<Node<T>> next() { return next; }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return data.equals(node.data) && next.equals(node.next);
        }

        @Override
        public int hashCode() {
            return Objects.hash(data, next);
        }
    }
}

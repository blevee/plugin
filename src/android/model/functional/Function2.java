package org.apache.cordova.mediacapture.model.functional;

public interface Function2<T, R, Q> {
    Q run(T t, R r);
}
//package org.apache.cordova.mediacapture.model.functional
//
//import org.apache.cordova.mediacapture.model.annotations.NonNull
//import kotlin.math.absoluteValue
//
//class ListK<T> {
//    fun <R> fold(f: (T, R) -> R, start: R): R {
//        var last = start
//        forEach { last = f(it, last) }
//        return last
//    }
//
//    fun forEach(f: (T) -> Unit) {
//    }
//}
//
//fun <T : Number> List<T>.closest(t: T): Double?  {
//    var closest: Double? = null
//    var lastDistance = Double.MAX_VALUE
//
//    forEach {
//        val distance = (t.toDouble() - t.toDouble()).absoluteValue
//        if (distance < lastDistance) {
//            lastDistance = distance
//            closest = it.toDouble()
//        }
//    }
//    return closest
//}
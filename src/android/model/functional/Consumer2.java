package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Consumer2<T, R> {
    void run(T t, R r);
}
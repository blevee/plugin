package org.apache.cordova.mediacapture.model.functional;

public class RangeClosed {
    int start;
    int end;

    public RangeClosed(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public boolean contains(int num) {
        for (int i = start; i <= end; i++)
            if (num == i)
                return true;
        return false;
    }

    public static RangeClosed createAround(int rangeCenter, int threshold) {
        return new RangeClosed(rangeCenter - threshold, rangeCenter + threshold);
    }
}

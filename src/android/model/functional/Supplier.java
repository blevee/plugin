package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Supplier<T> {
    T get();
}

package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Consumer1<T> {
    void run(T t);
}
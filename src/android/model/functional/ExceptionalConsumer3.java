package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface ExceptionalConsumer3<T, R, Q> {
    void run(T t, R r, Q q) throws Exception;
}

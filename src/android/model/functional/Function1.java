package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Function1<T, R> {
    R run(T t);
}
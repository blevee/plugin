package org.apache.cordova.mediacapture.model.functional;


import android.hardware.camera2.CameraAccessException;

@FunctionalInterface
public interface Consumer3<T, R, Q> {
    void run(T t, R r, Q q);
}
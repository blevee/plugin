package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Predicate1<T> {
    Boolean run(T t);
}

package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface ExceptionalConsumer2<T, R> {
    void run(T t, R r) throws Exception;
}
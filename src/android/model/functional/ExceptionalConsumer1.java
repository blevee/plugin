package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface ExceptionalConsumer1<T> {
    void run(T t) throws Exception;
}

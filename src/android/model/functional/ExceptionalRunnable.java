package org.apache.cordova.mediacapture.model.functional;

public interface ExceptionalRunnable {
    void run() throws Exception;
}

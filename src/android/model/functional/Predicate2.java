package org.apache.cordova.mediacapture.model.functional;

@FunctionalInterface
public interface Predicate2<T, R> {
    Boolean run(T t, R r);
}

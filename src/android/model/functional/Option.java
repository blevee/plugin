package org.apache.cordova.mediacapture.model.functional;

import org.apache.cordova.mediacapture.model.annotations.NonNull;
import org.apache.cordova.mediacapture.model.annotations.Nullable;

import java.util.Objects;

public class Option<T> {
    @Nullable
    private T tOrNull;

    private Option(T tOrNull) { this.tOrNull = tOrNull; }

    public static <T, R> void ifPresent(Option<T> o1, Option<R> o2, Consumer2<T, R> f) {
        o1.ifPresent(x1 -> o2.ifPresent(x2 -> f.run(x1, x2)));
    }

    public static <T, R, Q> void ifPresent(Option<T> o1, Option<R> o2, Option<Q> o3, Consumer3<T, R, Q> f) {
        o1.ifPresent(x1 -> o2.ifPresent(x2 -> o3.ifPresent(x3 -> f.run(x1, x2, x3))));
    }

    public static <T, R, Q, U> void ifPresent(Option<T> o1, Option<R> o2, Option<Q> o3, Option<U> o4, Consumer4<T, R, Q, U> f) {
        o1.ifPresent(x1 -> o2.ifPresent(x2 -> o3.ifPresent(x3 -> o4.ifPresent(x4 -> f.run(x1, x2, x3, x4)))));
    }


    public void ifPresent(Consumer1<T> f) {
        if (tOrNull != null)
            f.run(tOrNull);
    }

    public void ifEmpty(Runnable f) {
        if (tOrNull == null)
            f.run();
    }

    public <R> Option<R> map(Function1<T, R> f) {
        return isPresent() ? Option.of(f.run(tOrNull)) : Option.empty();
    }

    public <R> Option<R> flatMap(Function1<T, Option<R>> f) {
        return isPresent() ? f.run(tOrNull) : Option.empty();
    }

    public T or(T t) {
        return isPresent() ? tOrNull : t;
    }

    public Option<T> or(Option<T> t) {
        return tOrNull != null ? Option.of(tOrNull) : t;
    }

    public T getOrThrow() {
        if (isPresent())
            return tOrNull;
        else
            throw new RuntimeException("Attempted get on empty Option.");
    }

    public boolean contains(T t) {
        return tOrNull != null && tOrNull.equals(t);
    }

    public boolean isPresent() {
        return tOrNull != null;
    }

    public boolean isEmpty() {
        return tOrNull == null;
    }

    public static <T> Option<T> of(@NonNull T t) {
        return new Option<>(t);
    }

    public static <T> Option<T> empty() {
        return new Option<>(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option<?> option = (Option<?>) o;
        return Objects.equals(tOrNull, option.tOrNull);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tOrNull);
    }

    @Override
    public String toString() {
        return isPresent()
            ? "Option.of{" + tOrNull + "}"
            : "Option.empty()";
    }
}

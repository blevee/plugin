package org.apache.cordova.mediacapture.model.functional;


@FunctionalInterface
public interface Consumer4<T, R, Q, U> {
    void run(T t, R r, Q q, U u);
}
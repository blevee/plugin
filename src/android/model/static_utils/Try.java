package org.apache.cordova.mediacapture.model.static_utils;

import org.apache.cordova.mediacapture.model.functional.ExceptionalRunnable;

public class Try {
    public static void orThrow(ExceptionalRunnable f) {
        try {
            f.run();
        } catch(Exception e) {
            throw new RuntimeException(e.toString());
        }
    }

    public static void orThrow(ExceptionalRunnable f, Runnable g) {
        try {
            f.run();
        } catch(Exception e) {
            throw new RuntimeException(e.toString());
        } finally {
            g.run();
        }
    }
}

package org.apache.cordova.mediacapture.model.static_utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.cordova.mediacapture.model.functional.Function1;

public class Arrays {
    public static <T> boolean contains(T[] xs, T y) {
        for (T x : xs)
            if (Objects.equals(x, y))
                return true;
        return false;
    }

    public static <T> boolean missing(T[] xs, T y) {
        return !contains(xs, y);
    }

    public static <T> T[] append(Class<T> klass, T[] xs, T[] ys) {
        T[] result = (T[]) Array.newInstance(klass, xs.length + ys.length);
        System.arraycopy(xs, 0, result, 0, xs.length);
        System.arraycopy(ys, 0, result, 0 + xs.length, ys.length);
        return result;
    }
    public static <T> T[] join(Class<T> klass, T[]... xs) {
        T[] result = (T[]) Array.newInstance(klass, 0);
        for (T[] x : xs)
            result = append(klass, result, x);
        return result;
    }

    public static <T extends Number> T closest(T[] xs, T y) {
        T closest = xs[0];
        for (T x : xs)
            if (Math.abs(y.doubleValue() - x.doubleValue()) < Math.abs(closest.doubleValue() - x.doubleValue()))
                closest = y;
        return closest;
    }



    public static boolean rangeContains(int start, int end, int num) {
        for (int i = start; i <= end; i++)
            if (num == i)
                return true;
        return false;
    }

    public static boolean rangeAround(int rangeCenter, int threshold, int num) {
        return rangeContains(rangeCenter - threshold, rangeCenter + threshold, num);
    }

    // ~~ START CONVERSIONS ~~
    public static <T> String toString(T[] items) {
        StringBuilder sb = new StringBuilder();
        for (T item : items)
            sb.append(item).append(",");
        String str = sb.toString();
        return str.substring(0, str.length() - 1);
    }

    public static <T> List<T> toList(T[] items) {
        return java.util.Arrays.asList(items);
    }

    // ~~ END CONVERSIONS ~~

    // ~~ START HOFs ~~

    public static <T, R> R[] map(T[] items, Function1<T, R> f) {
        List<R> list = new ArrayList<>();

        for (T t : items)
            list.add(f.run(t));

        return (R[]) list.toArray();
    }

    // ~~ END HOFs ~~
}

package org.apache.cordova.mediacapture.model.static_utils;

import android.util.Size;

import org.apache.cordova.mediacapture.camera_activities.CameraActivity;
import org.apache.cordova.mediacapture.model.functional.Function1;
import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.model.functional.Option;
import org.apache.cordova.mediacapture.model.functional.Predicate2;

// NOTE: Internal components exist because at test-time `android.util.Size` doesn't exist for Cordova.
public class Sizes {
    public enum Type {
        PHOTO, VIDEO
    }

    public static class InternalSize {
        private final int width;
        private final int height;

        public InternalSize(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public InternalSize(Size s) {
            this.width = s.getWidth();
            this.height = s.getHeight();
        }

        public int getWidth() { return width; }
        public int getHeight() { return height; }
        public Size toAndroid() { return new Size(width, height); }

        @Override
        public String toString() {
            return "ISize{width=" + width + ", height=" + height + '}';
        }
    }

    public static Size chooseFileSize(List<Size> sizes, int exactDimension, Type type) {
        return Sizes.chooseFileSizeInternal(sizes.map(InternalSize::new), exactDimension, type, true).toAndroid();
    }

    public static InternalSize chooseFileSizeInternal(List<InternalSize> sizes, Integer dimension, Type type, boolean log) {
        Function1<InternalSize, Integer> getDimension;
        switch (type) {
            case PHOTO:
                getDimension = InternalSize::getWidth;
                break;
            case VIDEO:
                getDimension = InternalSize::getHeight;
                break;
            default:
                throw new RuntimeException("Some Dimension wasn't handled.");
        }

        Function1<Double, Option<InternalSize>> findSizeByRatio = ratio -> {
            Function1<Predicate2<Integer, Integer>, Option<InternalSize>> getSize = check ->
                List.closest(sizes.filter(s -> s.getWidth() == s.getHeight() * ratio && check.run(getDimension.run(s), dimension)), getDimension, dimension);

            Option<InternalSize> size   = getSize.run(Integer::equals);
            Option<InternalSize> higher = getSize.run((a, b) -> a > b);
            Option<InternalSize> lower  = getSize.run((a, b) -> a < b);
            return size.or(higher).or(lower);
        };

        Option<InternalSize> ratio4By3 = findSizeByRatio.run((double) 4 / 3);
        Option<InternalSize> ratio16By9 = findSizeByRatio.run((double) 16 / 9);

        if (log) {
            CameraActivity.writeToLog("Exact Dimension: " + dimension);
            CameraActivity.writeToLog("Type: " + type);
            CameraActivity.writeToLog("All Sizes: " + sizes);
            CameraActivity.writeToLog("4By3: " + ratio4By3);
            CameraActivity.writeToLog("16By9: " + ratio16By9);
        }

        switch (type) {
            case PHOTO:
                return ratio4By3.or(ratio16By9).or(sizes.last().getOrThrow());
            case VIDEO:
                return ratio16By9.or(ratio4By3).or(sizes.last().getOrThrow());
            default:
                throw new RuntimeException("Some Dimension wasn't handled.");
        }
    }

    public static Size choosePreviewSize(List<Size> sizes, int width, int height, Size aspectRatio) {
        int arW = aspectRatio.getWidth();
        int arH = aspectRatio.getHeight();

        return sizes
            .filter(s -> s.getHeight() == s.getWidth() * arH / arW && s.getWidth() >= width && s.getHeight() >= height)
            .minBy(s -> s.getWidth() * s.getHeight())
            .or(sizes.first().getOrThrow());
    }
}

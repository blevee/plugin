package org.apache.cordova.mediacapture.model.enumerations;

import android.widget.RelativeLayout;

import org.apache.cordova.mediacapture.model.functional.List;
import org.apache.cordova.mediacapture.model.functional.RangeClosed;

public enum Orientation {
    PORTRAIT(90, 0), LEFT_LANDSCAPE(0, 90), RIGHT_LANDSCAPE(180, 270);

    public int orientationHint;
    public int rotationForElements;

    private Orientation(int orientationHint, int rotationForElements) {
        this.orientationHint = orientationHint;
        this.rotationForElements = rotationForElements;
    }

    public void align(RelativeLayout.LayoutParams params) {
        List.of(
            RelativeLayout.ALIGN_PARENT_BOTTOM,
            RelativeLayout.ALIGN_PARENT_LEFT,
            RelativeLayout.ALIGN_PARENT_RIGHT
        ).forEach(params::removeRule);

        int rule;
        switch (this) {
            case LEFT_LANDSCAPE:
                rule = RelativeLayout.ALIGN_PARENT_LEFT;
                break;
            case RIGHT_LANDSCAPE:
                rule = RelativeLayout.ALIGN_PARENT_RIGHT;
                break;
            default:
                rule = RelativeLayout.ALIGN_PARENT_BOTTOM;
        }
        params.addRule(rule);
    }

    private static int THRESHOLD = 30;

    public static Orientation of(int orientation) {
        if (isLeftLandscape(orientation))
            return LEFT_LANDSCAPE;
        else if (isRightLandscape(orientation))
            return RIGHT_LANDSCAPE;
        else
            return PORTRAIT;
    }
    public static boolean isLeftLandscape(int orientation) {
        return RangeClosed.createAround(280, THRESHOLD).contains(orientation);
    }

    public static boolean isRightLandscape(int orientation) {
        return RangeClosed.createAround(90, THRESHOLD).contains(orientation);
    }

    public static boolean isPortrait(int orientation){
        return (orientation >= (360 - THRESHOLD) && orientation <= 360) || (orientation >= 0 && orientation <= THRESHOLD);
    }
}

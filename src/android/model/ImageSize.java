package org.apache.cordova.mediacapture.model;

public enum ImageSize {
    IMAGE_SIZE_2048x1024,
    IMAGE_SIZE_5376x2688
}

/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "HttpSession.h"
#import "CRSimplified-Swift.h"

@class HttpImageInfo;
@class CTPanoramaView;
 
#define KINT_HIGHT_INTERVAL_BUTTON  54

typedef enum : int {
    NoneInertia = 0,
    ShortInertia,
    LongInertia
} enumInertia;


@protocol RicohImageDelegate <NSObject>
    - (void) selectedPano: (UIImage *)panoSelected;
@end

@interface RicohImageViewController : UIViewController

@property (nonatomic, weak) id <RicohImageDelegate> delegate;
@property int panoQualityCompression;

@property (nonatomic, strong) CTPanoramaView *panoramaView;
@property (nonatomic, strong) IBOutlet UIView* panoFrame;
@property (nonatomic, strong) IBOutlet UITextView* textView;
@property (nonatomic, strong) IBOutlet UIProgressView* progressView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UIButton *selectPhotoButton;

- (IBAction)onCloseClicked:(id)sender;
- (void)getObject:(HttpImageInfo *)imageInfo withSession:(HttpSession *)session;
- (IBAction)onConfig:(id)sender;

@end

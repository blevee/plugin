//
//  CompassHelper.swift
//  CRSimplified
//
//  Created by Ryan Sjoquist on 9/20/19.
//

import Foundation

@objc(CompassHelper)

class CompassHelper: NSObject {
    
    @objc public var isLeft = true;
    
    
    class var swiftSharedInstance: CompassHelper {
        struct Singleton {
            static let instance = CompassHelper()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> CompassHelper {
        
        return CompassHelper.swiftSharedInstance
    }
    
    @objc public func findDegreesAndDirection(_ start: Double, current: Double) -> (Double) {
        var difference = abs(current - start).truncatingRemainder(dividingBy: 360)
        
        difference = difference > 180 ? (360 - difference) : difference
        if start > 180 || start == 0 && current < 180 {
            let range = abs(start - 180).truncatingRemainder(dividingBy: 360)
            if current > start || current < range {
                    isLeft = false
            } else if start < 180 {
                if current < start + 180 && current > start {
                    isLeft = true
                }
            }
        }
        return difference
    }
    
    
    
}

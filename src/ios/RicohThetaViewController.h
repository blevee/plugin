/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "RicohImageViewController.h"

@protocol CaptureRicohDelegate <NSObject>
    - (void) finishedImage: (UIImage *)ricohResult;
@end

@interface RicohThetaViewController : UIViewController
@property (nonatomic, weak) id <CaptureRicohDelegate> delegate;

@property (nonatomic, weak) RicohImageViewController *riVC;
@property int panoQualityCompression;
@property int timesTriedConnecting;
@property (nonatomic, strong) UIImage *selectedPano;
@property (nonatomic) NSString *modelId;
@property (nonatomic, strong) IBOutlet UITextField *ipField;
@property (nonatomic, strong) IBOutlet UIButton *connectButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *captureButton;
@property (strong, nonatomic) IBOutlet UIButton *imageSizeButtom;
@property (nonatomic, strong) IBOutlet UIImageView *motionJpegView;
@property (nonatomic, strong) IBOutlet UITableView *contentsView;
@property (nonatomic, strong) IBOutlet UITextView *logView;
@property (nonatomic) BOOL finishedPano;

@end

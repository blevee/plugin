/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface RicohTableCell : UITableViewCell

@property (nonatomic) uint32_t objectIndex;

@end

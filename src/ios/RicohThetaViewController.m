/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import "RicohThetaViewController.h"
#import "RicohTableCell.h"
#import "HttpConnection.h"
#import "RicohTableCellObject.h"


inline static void dispatch_async_main(dispatch_block_t block)
{
    dispatch_async(dispatch_get_main_queue(), block);
}

inline static void dispatch_async_default(dispatch_block_t block)
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);
}

@interface RicohThetaViewController () <UITableViewDelegate, UITableViewDataSource, RicohImageDelegate>
{
    HttpSession *session;
    HttpImageInfo * imageInfo;
    NSData* thumbData;
    NSArray * _imageInfoes;
    NSString* _callbackid;
    NSMutableArray* _objects;
    HttpStorageInfo* _storageInfo;
    NSNumber* _batteryLevel;
    HttpConnection* _httpConnection;
}
@property(nonatomic,weak)IBOutlet UIView *toastView;

@end

@implementation RicohThetaViewController

@synthesize delegate;

- (void)appendLog:(NSString*)text
{
    [_logView setText:[NSString stringWithFormat:@"%@%@\n", _logView.text, text]];
    [_logView scrollRangeToVisible:NSMakeRange([_logView.text length], 0)];
}

#pragma mark - UI events.

- (IBAction)onCancelClicked:(id)sender
{
    [self disconnect];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onConnetClicked:(id)sender
{
    [_ipField resignFirstResponder];
    // Disable Connect button
    self.connectButton.enabled = NO;
    
    [self appendLog:[NSString stringWithFormat:@"connecting %@...", _ipField.text]];
    
    // Setup `target IP`(camera IP).
    // Product default is "192.168.1.1".
    [_httpConnection setTargetIp:@"192.168.1.1"];
    [_httpConnection getSessionID];
    
    [self loadImages];
}

-(void)loadImages {
    NSDate* date = [NSDate date];
    BOOL isConnected = [_httpConnection connected];
    while (TRUE)
    {
        
        if (isConnected)
        {
            [self enumerateImages];
            break;
        }
        
        if ([date timeIntervalSinceNow] < -5)
        {
            date = nil;
            [self retryAlert];
            break;
        }
        usleep(20000);
    }
}

-(void)retryAlert {
    UIAlertController* alertController = [[UIAlertController alloc]init];
    alertController.title = @"Could not connect to Ricoh Theta";
    alertController.message = @"Try again?";
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
        [self disconnect];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction* retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self->_httpConnection getSessionID];
        [self loadImages];
    }];
    [alertController addAction:retryAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)onCaptureClicked:(id)sender
{
    // Disable Capture button and Disconnect button
    
    UIButton *senderButton = sender;
    senderButton.enabled = NO;
    self.connectButton.enabled = NO;
    
    // Start shooting process
    self->imageInfo = [_httpConnection takePicture];
    if (self->imageInfo != nil) {
        RicohTableCellObject* object = [RicohTableCellObject objectWithInfo:self->imageInfo];
        self->thumbData = [_httpConnection getThumb:self->imageInfo.file_id];
        object.thumbnail = [UIImage imageWithData:self->thumbData];
        [_objects insertObject:object atIndex:0];
        
        NSIndexPath* pos = [NSIndexPath indexPathForRow:0 inSection:1];
        dispatch_async_main(^{
            [_contentsView insertRowsAtIndexPaths:@[pos]
                                 withRowAnimation:UITableViewRowAnimationRight];
            for (NSInteger i = 1; i < _objects.count; ++i) {
                NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:1];
                [_contentsView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
            }
            RicohTableCell* cell = [self->_contentsView cellForRowAtIndexPath:pos];
            [self performSegueWithIdentifier:@"postImageSegue" sender:cell];
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        // Enable Capture button and Disconnect button
        senderButton.enabled = YES;
        self.connectButton.enabled = YES;
    });
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    RicohTableCell* c = (RicohTableCell*)sender;
    RicohTableCellObject* o = [_objects objectAtIndex:c.objectIndex];
    
    if (CODE_JPEG == o.objectInfo.file_format) {
        self.riVC = [segue destinationViewController];
        self.riVC.modalPresentationStyle = UIModalPresentationFullScreen;
        self.riVC.delegate = self;
        self.riVC.panoQualityCompression = self.panoQualityCompression;
        RicohTableCell* cell = (RicohTableCell*)sender;
        dispatch_async_default(^{
            RicohTableCellObject* object = [self->_objects objectAtIndex:cell.objectIndex];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            NSString* file = [NSString stringWithFormat:@"%@",object.objectInfo.file_id];
            NSURL *url = [NSURL URLWithString:file];
            
            request = [NSMutableURLRequest requestWithURL:url];
            
            self->session = [[HttpSession alloc] initWithRequest:request];
            
            [self.riVC getObject:object.objectInfo withSession:self->session];
        });
    }
}

-(void) selectedPano:(UIImage *)panoSelected {
    [self.delegate finishedImage:panoSelected];
    [self dismissViewControllerAnimated:YES completion:^{
        [self disconnect];
    }];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"SegueViewControllerToImageViewControllerID"]) {
        RicohTableCell* c = (RicohTableCell*)sender;
        RicohTableCellObject* o = [_objects objectAtIndex:c.objectIndex];
        
        if (CODE_JPEG != o.objectInfo.file_format) {
            [self showToast];
            return NO;
        }
    }
    return YES;
}

#pragma mark - HTTP Operations.

- (void)disconnect
{
    [self appendLog:@"disconnecting..."];
    _imageInfoes = nil;
    self->session = nil;
    _objects = nil;
    _httpConnection = nil;
    _storageInfo = nil;
    self.delegate = nil;
    _motionJpegView = nil;
    _contentsView = nil;
    [_httpConnection close:^{
        // "CloseSession" and "Close" completion callback.
        
        dispatch_async_main(^{
            _captureButton.enabled = NO;
            _imageSizeButtom.enabled = NO;
            _motionJpegView.image = nil;
            [self appendLog:@"disconnected."];
            [_connectButton setTitle:@"Connect to Ricoh Camera" forState:UIControlStateNormal];
            [_objects removeAllObjects];
            [_contentsView reloadData];
        });
    }];
}

- (void)enumerateImages
{
    if (self.timesTriedConnecting < 5) {
        [_httpConnection update];
        
        [_httpConnection getDeviceInfo:^(const HttpDeviceInfo* info) {
            // "GetDeviceInfo" completion callback.
            self.modelId = info.model;
            
            dispatch_async(dispatch_get_main_queue(),^{
                [self appendLog:[NSString stringWithFormat:@"DeviceInfo:%@", info]];
            });
            
        }];
        
        if (_httpConnection.sessionId) {
            [_httpConnection update];
            
        } else {
            [_httpConnection getSessionID];
            
        }
        [self->_httpConnection setClientVersion:2];
        
        
        dispatch_async(dispatch_get_main_queue(),^{
            // Create "Waiting" indicator
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            indicator.color = [UIColor grayColor];
            
            dispatch_async(dispatch_get_main_queue(),^{
                // Set indicator to be displayed in the center of the table view
                float w = indicator.frame.size.width;
                float h = indicator.frame.size.height;
                float x = _contentsView.frame.size.width/2 - w/2;
                float y = _contentsView.frame.size.height/2 - h/2;
                indicator.frame = CGRectMake(x, y, w, h);
                
                // Start indicator animation
                [_contentsView addSubview:indicator];
                [indicator startAnimating];
            });
            
            // Get storage information.
            _storageInfo = [_httpConnection getStorageInfo];
            
            // Get Battery level.
            _batteryLevel = [_httpConnection getBatteryLevel];
            
            // Get object informations for primary images.
            _imageInfoes = [_httpConnection getImageInfoes];
            
            // Get thumbnail images for each primary images.
            NSUInteger maxCount = MIN(_imageInfoes.count, 30);
            for (NSUInteger i = 0; i < maxCount; ++i) {
                HttpImageInfo *info = [_imageInfoes objectAtIndex:i];
                RicohTableCellObject* object = [RicohTableCellObject objectWithInfo:info];
                NSData* thumbData = [_httpConnection getThumb:info.file_id];
                object.thumbnail =[UIImage imageWithData:thumbData];
                [_objects addObject:object];
                
                dispatch_async(dispatch_get_main_queue(),^{
                    [self appendLog:[info description]];
                    [self appendLog:[NSString stringWithFormat:@"imageInfoes: %ld/%ld", i + 1, maxCount]];
                });
            }
            dispatch_async(dispatch_get_main_queue(),^{
                // Stop indicator animation
                [indicator stopAnimating];
                
                [_contentsView reloadData];
                
                // Enable Connect button
                self.connectButton.enabled = NO;
                _captureButton.enabled = YES;
                _imageSizeButtom.enabled = YES;
            });
            
            // Start live view display
            [_httpConnection startLiveView:^(NSData *frameData) {
                dispatch_async_main(^{
                    UIImage *image = [UIImage imageWithData:frameData];
                    _motionJpegView.image = image;
                    self.connectButton.enabled = false;
                    self.connectButton.alpha = 0;
                });
            }];
        });
        self.timesTriedConnecting = self.timesTriedConnecting+1;
    } else {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Please Reconnect Ricoh Theta"
                                     message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        //Add Buttons
        
        UIAlertAction* reconnectButton = [UIAlertAction
                                          actionWithTitle:@"Try Again"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
            //Handle your yes please button action here
            self.connectButton.enabled = NO;
            
            [self appendLog:[NSString stringWithFormat:@"connecting %@...", _ipField.text]];
            // Setup `target IP`(camera IP).
            // Product default is "192.168.1.1".
            [_httpConnection setTargetIp:@"192.168.1.1"];
            [_httpConnection getSessionID];
            //Preview
            [self enumerateImages];
        }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        //Add your buttons to alert controller
        [alert addAction:reconnectButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - UITableViewDataSource delegates.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return [_httpConnection connected] ? 1: 0;
    }
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    RicohTableCell* cell;
    
    if (indexPath.section==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cameraInfo"];
        cell.textLabel.text = [NSString stringWithFormat:@"%ld[shots] %ld/%ld[MB] free",
                               _storageInfo.free_space_in_images,
                               _storageInfo.free_space_in_bytes/1024/1024,
                               _storageInfo.max_capacity/1024/1024];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"BATT %.0f %%", [_batteryLevel doubleValue]*100.0];
    } else {
        // NSDateFormatter to display photographing date.
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        [df setDateStyle:NSDateFormatterShortStyle];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        
        RicohTableCellObject* obj = [_objects objectAtIndex:indexPath.row];
        cell = [tableView dequeueReusableCellWithIdentifier:@"customCell"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", obj.objectInfo.file_name];
        cell.detailTextLabel.text = [df stringFromDate:obj.objectInfo.capture_date];
        cell.imageView.image = obj.thumbnail;
        cell.objectIndex = (uint32_t)indexPath.row;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSIndexPath *path = indexPath;
    NSArray *pathArray = [NSArray arrayWithObject:path];
    dispatch_async_default(^{
        RicohTableCellObject *object = [_objects objectAtIndex:path.row];
        if ([_httpConnection deleteImage:object.objectInfo]) {
            dispatch_async_main(^{
                // Delete data source
                [_objects removeObjectAtIndex:path.row];
                // Delete row from table
                [_contentsView deleteRowsAtIndexPaths:pathArray
                                     withRowAnimation:UITableViewRowAnimationAutomatic];
                for (NSInteger i = path.row; i < _objects.count; ++i) {
                    NSIndexPath* index = [NSIndexPath indexPathForRow:i inSection:path.section];
                    [_contentsView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationTop];
                }
            });
        }
    });
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


#pragma mark - Life cycle.

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.timesTriedConnecting = 0;
    self.finishedPano = NO;
    _objects = [NSMutableArray array];
    _imageInfoes = [[NSArray alloc] init];
    _httpConnection = [[HttpConnection alloc] init];
    _contentsView.dataSource = self;
    _logView.layoutManager.allowsNonContiguousLayout = NO;
    _captureButton.enabled = NO;
    _imageSizeButtom.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [_httpConnection close:^{
    }];
    
    _httpConnection = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    if ([_httpConnection connected]) {
        self.connectButton.enabled = false;
        self.connectButton.alpha = 0;
    } else {
        self.connectButton.enabled = true;
        self.connectButton.alpha = 1;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)showToast
{
    self.toastView.hidden = NO;
    self.toastView.alpha = 1.0f;
    [UIView animateWithDuration:0.7f delay:3.0f options:(UIViewAnimationOptionAllowUserInteraction) animations:^{
        self.toastView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.toastView.hidden = YES;
        self.toastView.alpha = 1.0f;
    }];
}

@end

/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import "RicohTableCellObject.h"

@implementation RicohTableCellObject

+ (id)objectWithInfo:(HttpImageInfo*)info
{
    RicohTableCellObject *object = [[RicohTableCellObject alloc] init];
    object.objectInfo = info;
    return object;
}

@end

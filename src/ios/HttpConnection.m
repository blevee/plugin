/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import "HttpConnection.h"
#import "HttpStatusTimer.h"
#import "HttpFileList.h"
#import "HttpStream.h"

/**
 * HTTP connection to device
 */
@interface HttpConnection ()
{
    NSString *_server;
    NSURLSession *_session;
    NSMutableArray *_infoArray;
    HttpStream *_stream;
}
@end

@implementation HttpConnection

#pragma mark - Accessors.

/**
 * Specify address of connection destination
 * @param address Address
 */
- (void)setTargetIp:(NSString* const)address;
{
    
    _server = address;
}

/**
 * Status of connection to device
 * @return YES:Connect, NO:Disconnect
 */
- (BOOL)connected
{
    return (self.sessionId != nil);
}

#pragma mark - Life cycle.

/**
 * Initializer
 * @return Instance
 */
- (id)init
{
    if (self = [super init]) {
        // Timeout settings
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.timeoutIntervalForRequest = 5.0;

        _session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

#pragma mark - HTTP Connections.

/**
 * Notify device of continuation of session
 */
- (void)update
{
    if (self.sessionId) {
        // Create the url-request.
        NSMutableURLRequest *request = [self createExecuteRequest];

        // Create JSON data
        NSDictionary *body = @{@"name":@"camera.updateSession"
        };
        NSData *json = [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];

        // Set the request-body.
        [request setHTTPBody:json];

        // Send the url-request.
        NSURLSessionDataTask* task =
        [_session dataTaskWithRequest:request
                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        NSString *newId = nil;
                        if (!error) {
                            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                            newId = [array valueForKeyPath:@"results.sessionId"];
                            NSLog(@"result: %@", newId);
                        } else {
                            NSLog(@"error: %@", error);
                        }

                        if (newId) {
                            self.sessionId = newId;
                        }
                    }];
        [task resume];
    } else {
        
    }
}

/**
 * Disconnect from device
 * @param block Block called after disconnection process
 */
- (void)close:(void(^ const)())block
{
    if (self.sessionId) {
        // Stop live view
        [_stream cancel];

        // Create the url-request.
        NSMutableURLRequest *request = [self createExecuteRequest];

        // Create JSON data
        NSDictionary *body = @{
          @"name":@"camera.closeSession",
          @"parameters":
              @{ @"sessionId":self.sessionId }
        };
        NSData *json = [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];

        // Set the request-body.
        [request setHTTPBody:json];

        // Send the url-request.
        NSURLSessionDataTask* task =
        [self->_session dataTaskWithRequest:request
                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                             block();
                         }];
        [task resume];
        self.sessionId = nil;
    }
}

- (void)setClientVersion:(int)clientVersion {

    NSNumber *version = [[NSNumber alloc]initWithInt:clientVersion];
    if (self.sessionId == nil) {
            [self getSessionID];
            [self setOptions:@{@"clientVersion": version}];
    } else {
        [self update];
        [self setOptions:@{@"clientVersion": version}];
    }
}
/**
 * Acquire device information
 * @param block Block to be called after acquisition process
 */
- (void)getDeviceInfo:(void(^const )(const HttpDeviceInfo* const info))block
{
    // Create the url-request.
    NSMutableURLRequest *request = [self createRequest:@"/osc/info" method:@"GET"];

    // Do not set body for GET requests

    // Send the url-request.
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          HttpDeviceInfo* info = [[HttpDeviceInfo alloc] init];
                          if (!error) {
                              if (data) {
                                  NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                  NSString* apiLevel = [array valueForKey:@"apiLevel"];
                                  info.model = [array valueForKeyPath:@"model"];
                                  info.firmware_version = [array valueForKeyPath:@"firmwareVersion"];
                                  info.serial_number = [array valueForKeyPath:@"serialNumber"];
                                  NSLog(@"result: %@", info);
                              }
                          } else {
                              NSLog(@"error: %@", error);
                          }
                          block(info);
                      }];
    [task resume];
}

/**
 * Acquire list of media files on device
 * @return Media file list
 */
- (NSArray*)getImageInfoes
{
    // Create the url-request.
    NSMutableURLRequest *request = [self createExecuteRequest];
    HttpFileList *fileList = [[HttpFileList alloc] initWithRequest:request withSessionId:self.sessionId];
    NSUInteger* token;
    do {
        token = [fileList getList:10];
    } while (token);
    return fileList.infoArray;
}

/**
 * Acquire thumbnail image
 * @param fileId File ID
 * @return Thumbnail
 */
- (NSData*)getThumb:(NSString*)fileId
{
    // Semaphore for synchronization (cannot be entered until signal is called)
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

        // Create the url-request.
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];

        if(fileId != NULL){
            NSString* thumbFile = [NSString stringWithFormat:@"%@%@",fileId,@"?type=thumb"];
           // request = [self createRequest:thumbFile method:@"GET"];
            
            NSURL *url = [NSURL URLWithString:thumbFile];
            
            // Create the url-request.
            request = [NSMutableURLRequest requestWithURL:url];
            
            // Set the method(HTTP-POST)
            [request setHTTPMethod:@"GET"];
            
            [request setValue:@"application/json; charaset=utf-8" forHTTPHeaderField:@"Content-Type"];

        }
        
        // Create JSON data
    /*    NSDictionary *body = @{@"name": @"camera.listFiles",
                               @"parameters":
                                   @{@"fileUrl": fileId,   // ID of file to be acquired
                                     @"_type": @"thumb"}}; // Type of file to be acquired
        // Set the request-body.
        [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];
    */
        __block NSData *output;

        // Send the url-request.
        NSURLSessionDataTask* task =
        [self->_session dataTaskWithRequest:request
                          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                              if (!error) {
                                  if (data) {

                                      output = data;
                                      NSLog(@"result: %@", response);
                                  }
                              } else {
                                  NSLog(@"error: %@", error);
                              }
                              dispatch_semaphore_signal(semaphore);
                          }];
        [task resume];

        // Wait until signal is called
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        return output;
}

- (void)getSessionID {
    if (self.sessionId) {
        [self update];
    } else {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    NSMutableURLRequest *request = [self createExecuteRequest];
    NSDictionary *body = @{@"name": @"camera.startSession",
                           @"parameters": @{}
    };
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSInteger statusCode = 0;

            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                statusCode = httpResponse.statusCode;
            }
            if  (statusCode == 200) {
                NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                NSDictionary *retreivedData = [array valueForKeyPath:@"results"];
                self.sessionId = [retreivedData valueForKey:@"sessionId"];
            } else {
                self.sessionId = @"SID_0001";
                NSLog(@"400 %@",response);
            }
            
        } else {
            NSLog(@"%@",error);
        }
        dispatch_semaphore_signal(semaphore);

    }];
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }

}

/**
 * Acquire storage information of device
 * @return Storage informationget
 */
- (HttpStorageInfo*)getStorageInfo
{
    [self getSessionID];
    // Set still image as shooting mode (to acquire size set for still images)
    // Continue session
    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    // Create the url-request.
    NSMutableURLRequest *request = [self createExecuteRequest];

    // Create JSON data
    NSDictionary *body = @{@"name": @"camera.getOptions",
                           @"parameters":
                               @{
                                 @"optionNames":
                                     @[@"remainingPictures",
                                       @"remainingSpace",
                                       @"totalSpace",
                                       @"fileFormat",
                                       @"clientVersion"]}};

    // Set the request-body.
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];

    __block HttpStorageInfo *info = [[HttpStorageInfo alloc] init];

    // Send the url-request.
    NSURLSessionDataTask* task =
        [self->_session dataTaskWithRequest:request
              completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                  if (!error) {
                      if (data) {

                      // Acquire storage information
                      NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                      NSArray *options = [array valueForKeyPath:@"results.options"];
                      info.free_space_in_images = [[options valueForKey:@"remainingPictures"] unsignedLongValue]; // Number of images
                      info.free_space_in_bytes = [[options valueForKey:@"remainingSpace"] unsignedLongValue];     //byte
                      info.max_capacity = [[options valueForKey:@"totalSpace"] unsignedLongValue];                //byte

                      // Acquire file format setting
                      NSArray *fileFormat = [options valueForKey:@"fileFormat"];
                      info.image_width = [[fileFormat valueForKey:@"width"] unsignedLongValue];
                      info.image_height = [[fileFormat valueForKey:@"height"] unsignedLongValue];
                      NSLog(@"result: %@", array);
                      }
                  } else {
                      NSLog(@"error: %@", error);
                  }
                  dispatch_semaphore_signal(semaphore);
              }];
    [task resume];
    
    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return info;
}

/**
 * Acquire battery information of device
 * @return Battery level (4 levels: 0.0, 0.33, 0.67 and 1.0)
 */
-(NSNumber*)getBatteryLevel
{
    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    // Create the url-request.
    NSMutableURLRequest *request = [self createRequest:@"/osc/state" method:@"POST"];

    __block NSNumber *batteryLevel;

    // Send the url-request.
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          if (!error) {
                              
                              NSArray* array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                              NSArray* state = [array valueForKey:@"state"];
                              batteryLevel = [state valueForKey:@"batteryLevel"];
                              NSLog(@"result: %@", batteryLevel);
                          } else {
                              NSLog(@"error: %@", error);
                          }
                          dispatch_semaphore_signal(semaphore);
                      }];
    [task resume];

    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return batteryLevel;
}

/**
 * Specify shooting size
 * @param width Width of shot image
 * @param height Height of shot image
 */
- (void)setImageFormat:(NSUInteger)width height:(NSUInteger)height
{
    [self setOptions:@{@"captureMode": @"image"}];
    [self setOptions:@{@"fileFormat":
                           @{@"type": @"jpeg",
                             @"width": [NSNumber numberWithUnsignedInteger:width],
                             @"height": [NSNumber numberWithUnsignedInteger:height]}}];
}

/**
 * Start live view
 * @param block Block called on drawing. Used to perform the drawing process of the image.
 */
- (void)startLiveView:(void(^ const)(NSData *frameData))block
{
    NSMutableURLRequest *request = [self createExecuteRequest];
        _stream = [[HttpStream alloc] initWithRequest:request];
        [_stream setDelegate:block];
        [_stream getData];
}

/**
 * Resume live view
 */
- (void)restartLiveView
{
    [_stream getData];
}

/**
 * Take photo<p>
 * After shooting, the status is checked by the timer and the file information is acquired when the status indicates that the process is complete.
 * @return Information on shot media files
 */
- (HttpImageInfo*)takePicture
{
    // Stop live view
    [_stream cancel];

    // Set still image as shooting mode
    // Continue session

    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    // Create the url-request.
    NSMutableURLRequest *request = [self createExecuteRequest];

    // Create JSON data
    NSDictionary *body = @{@"name": @"camera.takePicture",
                           @"parameters":
                           @{@"sessionId": self.sessionId}
    };

    // Set the request-body.
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];

    __block NSString *commandId;

    // Send the url-request.
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          if (!error) {
                              NSArray* array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                              commandId = [array valueForKey:@"id"];
                              NSLog(@"commandId: %@", commandId);
                          } else {
                              NSLog(@"error: %@", error);
                          }
                          dispatch_semaphore_signal(semaphore);
                      }];
    [task resume];

    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    HttpImageInfo *resultInfo = [self waitCommandComplete:commandId];

    // Resume live view
    [_stream getData];

    return resultInfo;
}

/**
 * Check status of specified command and acquire information
 * @param commandId ID of command to be checked
 */
- (HttpImageInfo*)waitCommandComplete:(NSString*)commandId
{
    if (commandId != nil) {
        // Create timer and wait until process is completed
        NSMutableURLRequest *requestForStatus = [self createRequest:@"/osc/commands/status" method:@"POST"];
        HttpStatusTimer *timer = [[HttpStatusTimer alloc] initWithRequest:requestForStatus];
        NSString *status = [timer run:commandId];

        if ([status isEqualToString:@"done"]) {
            // Create the url-request.
            NSMutableURLRequest *requestForList = [self createExecuteRequest];
            HttpFileList *fileList = [[HttpFileList alloc] initWithRequest:requestForList withSessionId:self.sessionId];

            NSUInteger* token;
            do {
                token = [fileList getPhoto:timer.fileUrl];
                HttpImageInfo *info = fileList.infoArray.firstObject;
                if ([info.file_id isEqualToString:timer.fileUrl]) {
                    return info;
                }
            } while (token != 0);
        }
    }
    return nil;
}

/**
 * Delete specified file
 * @param info Information of file to be deleted
 * @return Delete process successful?
 */
- (BOOL)deleteImage:(HttpImageInfo*)info
{
    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    // Create the url-request.
    NSMutableURLRequest *request = [self createExecuteRequest];

    // Create JSON data
    NSDictionary *body = @{@"name": @"camera.delete",
                           @"parameters":
                               @{@"fileUri": info.file_id,
                                 @"sessionId": self.sessionId
                               }};

    // Set the request-body.
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];

    __block NSString *status;
    __block NSString *commandId = nil;

    // Send the url-request.
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          if (!error) {
                              NSArray* array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                              status = [array valueForKey:@"state"];
                              commandId = [array valueForKey:@"id"];
                              NSLog(@"commandId: %@", commandId);
                          } else {
                              NSLog(@"error: %@", error);
                          }
                          dispatch_semaphore_signal(semaphore);
                      }];
    [task resume];

    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    if ([status isEqualToString:@"done"]) {
        return YES;
    } else if (commandId != nil) {
        // Create timer and wait until process is completed
        NSMutableURLRequest *requestForStatus = [self createRequest:@"/osc/commands/status" method:@"POST"];
        HttpStatusTimer *timer = [[HttpStatusTimer alloc] initWithRequest:requestForStatus];
        status = [timer run:commandId];
        if ([status isEqualToString:@"done"]) {
            return YES;
        }
    }
    return NO;
}

/**
 * Create HTTP request class instance for executing command
 * @return HTTP request class instance for executing command
 */
- (NSMutableURLRequest*)createExecuteRequest
{
    // Create the url-request.
    return [self createRequest:@"/osc/commands/execute" method:@"POST"];
}


#pragma mark - Private methods.

/**
 * Send option setting request
 * @param options Dictionary in which the option name and settings were configured for the key and value
 */
- (void)setOptions:(NSDictionary*)options
{

    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    // Create the url-request.
    NSMutableURLRequest *request = [self createExecuteRequest];

    // Create JSON data
    NSDictionary *body = @{@"name": @"camera.setOptions",
                           @"parameters":
                               @{  @"sessionId":self.sessionId,
                                   @"options":options}};
    NSLog(@"request %@", body);
    // Set the request-body.
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil]];

    // Send the url-request.
    NSURLSessionDataTask* task =
    [self->_session dataTaskWithRequest:request
                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                          if (!error) {
                              NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                              NSLog(@"result: %@ %@", response, array);
                          } else {
                              NSLog(@"error: %@ %@", error, response);
                          }
                          dispatch_semaphore_signal(semaphore);
                      }];
    [task resume];

    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

/**
 * Create HTTP request
 * @param protocol Path
 * @param method Protocol
 * @return HTTP request instance
 */
- (NSMutableURLRequest*)createRequest:(NSString* const)protocol method:(NSString* const)method
{
    NSString *string = [NSString stringWithFormat:@"http://%@%@", _server, protocol];
    NSURL *url = [NSURL URLWithString:string];

    // Create the url-request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    // Set the method(HTTP-POST)
    [request setHTTPMethod:method];

    [request setValue:@"application/json; charaset=utf-8" forHTTPHeaderField:@"Content-Type"];

    return request;
}

@end

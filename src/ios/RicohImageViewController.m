/*
 * Copyright Ricoh Company, Ltd. All rights reserved.
 */

#import "RicohImageViewController.h"
#import "HttpImageInfo.h"
#import "CDVCapture.h"
#import "Bridging-Header.h"

@interface RicohImageViewController ()
{
    HttpImageInfo *_httpImageInfo;
    HttpSession *_session;
    UIImage *_image;
    NSMutableData *_imageData;
    int imageWidth;
    int imageHeight;
    float _yaw;
    float _roll;
    float _pitch;
}
@end

@implementation RicohImageViewController

@synthesize delegate;

- (void)appendLog:(NSString*)text
{
    [_textView setText:[NSString stringWithFormat:@"%@%@\n", _textView.text, text]];
    [_textView scrollRangeToVisible:NSMakeRange([_textView.text length], 0)];
}

#pragma mark - UI events

- (void)onCloseClicked:(id)sender
{
    [self tearDown];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)myCloseClicked:(id)sender
{
    [self tearDown];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tearDown {
    
    [self.panoramaView cleanUp];
    [self.panoramaView removeFromSuperview];
    self.delegate = nil;
    self.panoramaView.image = nil;
    self.panoramaView = nil;
    _image = nil;
    _imageData = nil;
    _session = nil;
    _httpImageInfo = nil;
    _textView = nil;
    _progressView = nil;
    _closeButton = nil;
    _selectPhotoButton = nil;
}

- (IBAction)selectPhoto:(id)sender {
    [self.delegate selectedPano:_image];

    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - HTTP Operation

- (void)getObject:(HttpImageInfo *)imageInfo withSession:(HttpSession *)session
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _progressView.progress = 0.0;
        _progressView.hidden = NO;
    });
    
    _httpImageInfo = imageInfo;
    _session = session;
    NSString *fileUrl = imageInfo.file_id;
    // Semaphore for synchronization (cannot be entered until signal is called)
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [session getResizedImageObject:fileUrl
                           onStart:^(int64_t totalLength) {
        // Callback before object-data reception.
        NSLog(@"getObject(%@) will received %zd bytes.", fileUrl, totalLength);
    }
                           onWrite:^(int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        // Callback for each chunks.
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update progress.
            self->_progressView.progress = (float)totalBytesWritten / totalBytesExpectedToWrite;
        });
    }
                          onFinish:^(NSURL *location){
        self->_imageData = [NSMutableData dataWithContentsOfURL:[NSURL URLWithString:fileUrl]];
        self->_image = [UIImage imageWithData:self->_imageData];
        self->_selectPhotoButton.hidden = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    
    // Wait until signal is called
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    

    // If there is no information, yaw, pitch and roll method returns NaN.
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _progressView.hidden = YES;
        self.panoramaView.image = self->_image;
        [session invalidateAndClose];
        _session = nil;
        self.panoramaView.controlMethod = CTPanoramaControlMethodTouch;
        self.panoramaView.panoramaType = CTPanoramaTypeSpherical;
        _imageData = nil;
        
    });
}

#pragma mark - Life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}



- (void)viewDidDisappear:(BOOL)animated {
    [self tearDown];

    [super viewDidDisappear:animated];
}


- (void)viewWillAppear:(BOOL)animated {
    self.panoramaView = [[CTPanoramaView alloc]initWithFrame: self.panoFrame.frame];
    [self.view addSubview:self.panoramaView];
    [self.view setNeedsLayout];
        
    if (nil != _httpImageInfo && CODE_JPEG == _httpImageInfo.file_format) {
        _progressView.hidden = NO;
        _selectPhotoButton.hidden = YES;
    }
    else {
        _progressView.hidden = YES;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    _textView.text = nil;
}


@end
